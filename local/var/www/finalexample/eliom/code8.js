function start () {
  var  cy = window.cy = cytoscape({
    container: document.getElementById('cy'),
    layout: {
      name: 'grid',
      rows: 2,
      cols: 2
    },
    style: [
      {
        selector: 'node[name]',
        style: {
          'content': 'data(name)',
          'width': '40px',
          'height': '40px',
          'text-valign': 'bottom',
          'text-halign': 'center'
        }
      },
      {
        selector: 'edge[symbol]',
        style: {
          'content': 'data(symbol)'
        }
      },
      {
        selector: 'edge',
        style: {
          'curve-style': 'bezier',
          'target-arrow-shape': 'triangle'
        }
      },
    {
      selector: '#transparent',
      style: {
        'visibility': 'hidden'
      }
    },
    {
      selector: '.SUCCESS',
      style: {
        'border-width': '7px',
        'border-color': 'black',
        'border-style': 'double'
      }
  },
    ],
    elements: {
      nodes: [
        {data: { id: 'transparent', name: 'transparent' },  position: { x: 0, y: 0 }}
      ]
    }
  });
}

function makeNode (nm)  {
  var  verify = cy.getElementById (nm);
  if (verify.length < 1) {
    if (nm == 'START') {
      cy.add({
        data: { id: nm, name: nm },
        position: { x: 100, y: 0 }
      });
      makeEdge ('transparent', nm, '')
    } else {
        cy.add({
          data: { id: nm, name: nm },
          position: { x: Math.floor(Math.random() * 1399), y: Math.floor(Math.random() * 299) }
        });
    }
    cy.fit();
  }
};

function makeNode1 (nm, final)  {
  var  verify = cy.getElementById (nm);
  console.log (final);
  if (verify.length < 1) { 
    if (final == "true") {
    if (nm == 'START') {
      cy.add({
        data: { id: nm, name: nm },
        position: { x: 100, y: 0 }, classes: 'SUCCESS'
      });
      makeEdge ('transparent', nm, '')
    } else {
        cy.add({
          data: { id: nm, name: nm },
          position: { x: Math.floor(Math.random() * 1399), y: Math.floor(Math.random() * 299) }, classes: 'SUCCESS'
        });
    }
    } else {
      if (nm == 'START') {
        cy.add({
          data: { id: nm, name: nm },
          position: { x: 100, y: 0 }
        });
        makeEdge ('transparent', nm, '')
      } else {
          cy.add({
            data: { id: nm, name: nm },
            position: { x: Math.floor(Math.random() * 1399), y: Math.floor(Math.random() * 299) }
          });
      }
    }
    cy.fit();
  }
};

function makeEdge (first, second, third)  {
  console.log (first + ", " + second + ", " + third);

  var nId = first + second;
  var getEdge = cy.getElementById(nId);

  if (getEdge.length  == 0) {
    cy.add({
      data: { id: nId, source: first, symbol: third, target: second }
    });
  } else {
    var k = getEdge.data('symbol');
    getEdge.remove();
    var newsymbol = k + ', ' + third;
    cy.add({
      data: { id: nId, source: first, symbol: newsymbol, target: second }
    })
  }

  cy.fit();
};

function destroy1 () {
  if (cy != null) {
    cy.destroy1;
  }

};

function changeColor1 (node, length, success) {
  console.log (node);
  var test = parseInt(length);
  console.log ("Tamanho da frase" + test);

  console.log (success);

  if (length != 0) {
  cy.style (cytoscape.stylesheet()
    .selector( 'node[name]')
    .style ({
      'content': 'data(name)',
      'text-valign': 'bottom',
      'text-halign': 'center'
    })
    .selector( 'edge[symbol]')
    .style ( {
          'content': 'data(symbol)'
        })
    .selector ('edge')
    .style ({
          'curve-style': 'bezier',
          'target-arrow-shape': 'triangle'
    })
    .selector('#' + node)
    .style({
      'background-color': 'blue'
    })
    .selector ('.SUCCESS')
    .style ({
          'border-width': '10px',
          'border-color': 'black',
          'border-style': 'double'
        })
    .selector( '#transparent')
    .style ({
          'visibility': 'hidden'
        })
  );
  } else {
    if (success == "true") {
      cy.style (cytoscape.stylesheet()
    .selector( 'node[name]')
    .style ({
      'content': 'data(name)',
      'text-valign': 'bottom',
      'text-halign': 'center'
    })
    .selector( 'edge[symbol]')
    .style ( {
          'content': 'data(symbol)'
        })
    .selector ('edge')
    .style ({
          'curve-style': 'bezier',
          'target-arrow-shape': 'triangle'
    })
    .selector('#' + node)
    .style({
      'background-color': 'green'
    })
    .selector ('.SUCCESS')
    .style ({
          'border-width': '10px',
          'border-color': 'black',
          'border-style': 'double'
        })
    .selector( '#transparent')
    .style ({
              'visibility': 'hidden'
            })
  );
    } else {
      cy.style (cytoscape.stylesheet()
    .selector( 'node[name]')
    .style ({
      'content': 'data(name)',
      'text-valign': 'bottom',
      'text-halign': 'center'
    })
    .selector( 'edge[symbol]')
    .style ( {
          'content': 'data(symbol)'
        })
    .selector ('edge')
    .style ({
          'curve-style': 'bezier',
          'target-arrow-shape': 'triangle'
    })
    .selector('#' + node)
    .style({
      'background-color': 'red'
    })
    .selector ('.SUCCESS')
    .style ({
          'border-width': '10px',
          'border-color': 'black',
          'border-style': 'double'
        })
        .selector( '#transparent')
        .style ({
              'visibility': 'hidden'
            })
  );
    }
  }
};

function resetStyle () {
  cy.style()
    .resetToDefault()
    .selector ('node[name]')
    .style ({'content': 'data(name)',
          'width': '40px',
          'height': '40px', 'text-valign': 'bottom',
          'text-halign': 'center'})
    .selector( 'edge[symbol]')
    .style ( {
                'content': 'data(symbol)'
              })
    .selector( 'edge')
    .style ({
            'curve-style': 'bezier',
            'target-arrow-shape': 'triangle'
          })
          .selector ('.SUCCESS')
          .style ({
                'border-width': '10px',
                'border-color': 'black',
                'border-style': 'double'
              })
            .selector( '#transparent')
            .style ({
                    'visibility': 'hidden'
                  })
    .update()
}

function paintProd (a) {
  cy.style ()
    .selector('#' + a)
    .style ( {
      'background-color': 'orange'
    })
    .update()
};

function paintReach (a) {
  cy.style ()
    .selector('#' + a)
    .style ( {
      'background-color': 'yellow'
    })
    .update()
}

function paintUn (a) {
  cy.style ()
    .selector('#' + a)
    .style ( {
      'background-color': 'purple'
    })
    .update()
}

function paintUseful (a) {
  cy.style ()
    .selector('#' + a)
    .style ( {
      'background-color': 'purple'
    })
    .update()
}